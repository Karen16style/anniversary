import 'package:flutter/material.dart';
import 'package:x/data/entities/weding_day.dart';
import 'package:x/presentation/ui/widgets/floating_button_widget.dart';

class AnniversaryOpenPage extends StatelessWidget {
  const AnniversaryOpenPage({Key key, this.wedingDay}) : super(key: key);

  final WedingDay wedingDay;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        bottom: PreferredSize(
          child: Column(
            children: [
              CircleAvatar(
                backgroundImage: AssetImage('assets/images/dash.jpg'),
                radius: 90,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  '${wedingDay.lastname}',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                  ),
                ),
              )
            ],
          ),
          preferredSize: const Size.fromHeight(200.0),
        ),
        title: Text(''),
      ),
      body: ListView(children: <Widget>[
        SafeArea(
          maintainBottomViewPadding: true,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('Дата годовщины'),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    '${wedingDay.anniversaryDate}',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ),
                Divider(),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('Идеи подарков и поздравлений'),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('${wedingDay.surpise}'),
                )
              ],
            ),
          ),
        )
      ]),
      floatingActionButton: buildSpeedDial(),
    );
  }
}
