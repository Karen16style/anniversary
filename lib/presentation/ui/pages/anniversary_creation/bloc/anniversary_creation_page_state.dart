part of 'anniversary_creation_page_bloc.dart';

@immutable
abstract class AnniversaryCreationPageState {}

class AnniversaryCreationPageInitial extends AnniversaryCreationPageState {

}
class PhotoSelectedState extends AnniversaryCreationPageState {
  final File freindsPhoto;

  PhotoSelectedState(this.freindsPhoto);
}

class DateSelectedState extends AnniversaryCreationPageState {
  final DateTime anniversaryDate;

  DateSelectedState(this.anniversaryDate);
}