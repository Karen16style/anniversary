part of 'anniversary_creation_page_bloc.dart';

@immutable
abstract class AnniversaryCreationPageEvent {}

class AddAnniversaryEvent extends AnniversaryCreationPageEvent {
  final String lastname;
  final DateTime anniversaryDate;
  final String surpise;
  final File freindsPhoto;
  final BuildContext context;

  AddAnniversaryEvent(
      {@required this.context,
      @required this.lastname,
      @required this.anniversaryDate,
      this.surpise,
      this.freindsPhoto});
}

class SetPhotoEvent extends AnniversaryCreationPageEvent {
  final File freindsPhoto;

  SetPhotoEvent({this.freindsPhoto});
}

class SetDateEvent extends AnniversaryCreationPageEvent {
  final DateTime anniversaryDate;

  SetDateEvent({this.anniversaryDate});
}
