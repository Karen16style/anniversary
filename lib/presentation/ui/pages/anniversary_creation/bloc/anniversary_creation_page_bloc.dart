import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:x/data/entities/weding_day.dart';
import 'package:x/domain/use_cases/use_case.dart';
import 'package:provider/provider.dart';

part 'anniversary_creation_page_event.dart';
part 'anniversary_creation_page_state.dart';

class AnniversaryCreationPageBloc
    extends Bloc<AnniversaryCreationPageEvent, AnniversaryCreationPageState> {
  AnniversaryCreationPageBloc() : super(AnniversaryCreationPageInitial());

  @override
  Stream<AnniversaryCreationPageState> mapEventToState(
    AnniversaryCreationPageEvent event,
  ) async* {
    if (event is AddAnniversaryEvent) {
      var wedingDay = WedingDay(
        anniversaryDate: event.anniversaryDate,
        lastname: event.lastname,
        surpise: event.surpise,
        freindsPhoto: event.freindsPhoto,
      );
      String errorText;
      final useCase = event.context.read<UseCase>();
      await useCase.anniversary.creatAnniversary(wedingDay).catchError((onError) {
        print(onError);
        errorText = onError.toString();
      });
      if (errorText != null) {
        ScaffoldMessenger.of(event.context)
            .showSnackBar(SnackBar(content: Text(errorText)));
      } else {
        Navigator.pop(event.context);
      }
    } else if (event is SetPhotoEvent) {
      yield PhotoSelectedState(event.freindsPhoto);
    } else if (event is SetDateEvent) {
      yield DateSelectedState(event.anniversaryDate);
    }
  }
}
