import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:x/presentation/ui/pages/anniversary_creation/bloc/anniversary_creation_page_bloc.dart';
import 'package:x/presentation/ui/widgets/image_picker_widget.dart';
import 'package:x/presentation/ui/widgets/date_picker_widget.dart';

class AnniversaryCreationPage extends StatefulWidget {
  @override
  _AnniversaryCreationPageState createState() =>
      _AnniversaryCreationPageState();
}

final _formKey = GlobalKey<FormState>();
final _lastnameContoller = TextEditingController();
final _surpriseContoller = TextEditingController();

class _AnniversaryCreationPageState extends State<AnniversaryCreationPage> {
  @override
  Widget build(BuildContext context) {
    File photo;
    DateTime anniversaryDate;

    return BlocProvider(
        create: (context) => AnniversaryCreationPageBloc(),
        child: BlocListener<AnniversaryCreationPageBloc,
                AnniversaryCreationPageState>(
            listener: (context, state) {
              if (state is PhotoSelectedState) {
                photo = state.freindsPhoto;
              }
              if (state is DateSelectedState) {
                anniversaryDate = state.anniversaryDate;
              }
            },
            child: BlocBuilder<AnniversaryCreationPageBloc,
                AnniversaryCreationPageState>(buildWhen: (previous, current) {
              return current is! PhotoSelectedState;
            }, builder: (context, state) {
              return Scaffold(
                appBar: AppBar(
                  title: Text('Добавить'),
                  actions: <Widget>[
                    IconButton(
                      icon: Icon(Icons.done),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          context
                              .read<AnniversaryCreationPageBloc>()
                              .add(AddAnniversaryEvent(
                                anniversaryDate: anniversaryDate,
                                lastname: _lastnameContoller.text,
                                surpise: _surpriseContoller.text,
                                freindsPhoto: photo,
                                context: context,
                              ));
                        }
                      },
                    ),
                  ],
                ),
                body: SingleChildScrollView(
                  child: SafeArea(
                    maintainBottomViewPadding: true,
                    child: Form(
                      key: _formKey,
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.all(8.0),
                                        child: TextFormField(
                                          controller: _lastnameContoller,
                                          textCapitalization:
                                              TextCapitalization.sentences,
                                          decoration: InputDecoration(
                                              hintText: 'Петровы',
                                              border: OutlineInputBorder(),
                                              prefixIcon: Icon(Icons.group),
                                              labelText: 'Фамилия семьи*'),
                                          keyboardType: TextInputType.name,
                                          maxLength: 20,
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return 'Пожалуйста введите Фамилию';
                                            }
                                            return null;
                                          },
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: DatePickerWidget(),
                                      ),
                                    ],
                                  ),
                                ),
                                ImagePickerWidget(),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: TextFormField(
                                controller: _surpriseContoller,
                                decoration: InputDecoration(
                                    hintText: 'Деньги, Золото, 5кг Икры',
                                    border: OutlineInputBorder(),
                                    prefixIcon: Icon(Icons.redeem),
                                    labelText: 'Идеи подарков и поздравлений'),
                                keyboardType: TextInputType.name,
                                maxLength: 500,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              );
            })));
  }
}
