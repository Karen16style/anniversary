import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:x/data/entities/weding_day.dart';
import 'package:x/domain/use_cases/use_case.dart';

import 'package:x/presentation/ui/pages/anniversary_creation/anniversary_creation_page.dart';
import 'package:x/presentation/ui/pages/anniversary_open/anniversary_open_page.dart';
import 'package:x/presentation/ui/pages/settings/settings_page.dart';
import 'package:x/presentation/ui/widgets/anniversary_widget.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    timeDilation = 2.0;
    return Scaffold(
      appBar: AppBar(
        title: Text('Годовщины'),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.settings,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SettingPage()),
              );
            },
          ),
        ],
      ),
      body: SafeArea(
        child: Container(
            height: MediaQuery.of(context).size.height - 85,
            decoration: BoxDecoration(),
            child: ListViewWidget()),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => AnniversaryCreationPage()),
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }
}

class ListViewWidget extends StatefulWidget {
  @override
  _ListViewWidgetState createState() => _ListViewWidgetState();
}

class _ListViewWidgetState extends State<ListViewWidget> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<WedingDay>>(
      stream: context.read<UseCase>().anniversary.readAnniversary(),
      builder: (context, snapshot) {
        return snapshot.hasData
            ? ListView.builder(
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.vertical,
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  final item = snapshot.data[index];

                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              AnniversaryOpenPage(wedingDay: item),
                        ),
                      );
                    },
                    child: AnniversaryWidget(wedingDay: item),
                  );
                })
            : Center(
                child: CircularProgressIndicator(),
              );
      },
    );

    // return ListView.builder(
    //   physics: BouncingScrollPhysics(),
    //   scrollDirection: Axis.vertical,
    //   itemCount: 10,
    //   itemBuilder: (BuildContext context, int index) {
    //     return GestureDetector(
    //       onTap: () {
    //         Navigator.push(
    //           context,
    //           MaterialPageRoute(
    //             builder: (context) => AnniversaryOpenPage(
    //               wedingDay: WedingDay(
    //                   anniversaryDate: DateTime(2015, 11, 11),
    //                   lastname: 'Варданяны',
    //                   surpise: 'Денег побольше'),
    //             ),
    //           ),
    //         );
    //       },
    //       child: AnniversaryWidget(
    //         wedingDay: WedingDay(
    //             anniversaryDate: DateTime.now(), lastname: 'Варданяны'),
    //       ),
    //     );
    //   },
    // );
  }
}
