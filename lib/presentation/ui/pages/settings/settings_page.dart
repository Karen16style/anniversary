import 'package:flutter/material.dart';
import 'package:x/presentation/ui/widgets/hour_picker_widget%20copy.dart';

class SettingPage extends StatelessWidget {
  const SettingPage({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Настройки'),
      ),
      body: SafeArea(
        maintainBottomViewPadding: true,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            RawMaterialButton(
              padding: const EdgeInsets.all(16.0),
              onPressed: () {},
              child: Row(
                children: [
                  Icon(
                    Icons.lock_open,
                    color: Colors.redAccent,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Text('Премиум версия',
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                        )),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text('Оповещения',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.redAccent,
                  )),
            ),
            HourPickerWidget(),
            Divider(),
            RawMaterialButton(
              padding: const EdgeInsets.all(16.0),
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return Dialog(
                        child: Container(
                          width: 300,
                          height: 300,
                        ),
                      );
                    });
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text('Дополнительные напоминания',
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                      )),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text('В',
                        style: TextStyle(
                          color: Colors.grey,
                          fontWeight: FontWeight.w300,
                        )),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text('Идеи подарков и поздравлений'),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(''),
            )
          ],
        ),
      ),
    );
  }
}
