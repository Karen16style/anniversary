import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

SpeedDial buildSpeedDial() {
  return SpeedDial(
    animatedIcon: AnimatedIcons.menu_close,
    animatedIconTheme: IconThemeData(size: 22.0),
    icon: Icons.add,
    curve: Curves.bounceIn,
    children: [
      SpeedDialChild(
        child: Icon(Icons.delete_outline, color: Colors.white),
        backgroundColor: Colors.red.shade400,
        onTap: () => print('FIRST CHILD'),
        label: 'Удалить',
        labelStyle: TextStyle(fontWeight: FontWeight.w500),
        labelBackgroundColor: Colors.red.shade400,
      ),
      SpeedDialChild(
        child: Icon(Icons.create_outlined, color: Colors.white),
        backgroundColor: Colors.green,
        onTap: () => print('SECOND CHILD'),
        label: 'Редактировать',
        labelStyle: TextStyle(fontWeight: FontWeight.w500),
        labelBackgroundColor: Colors.green,
      ),
    ],
  );
}
