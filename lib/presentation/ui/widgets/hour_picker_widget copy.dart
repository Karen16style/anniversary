import 'package:flutter/material.dart';

class HourPickerWidget extends StatefulWidget {
  @override
  _HourPickerWidgetState createState() => _HourPickerWidgetState();
}

class _HourPickerWidgetState extends State<HourPickerWidget> {
  TimeOfDay _time;

  void _pickTime() async {
    TimeOfDay time = await showTimePicker(
      context: context,
      initialTime: _time,
      helpText: 'Выберите время напоминания',
    );
    if (time != null)
      setState(() {
        _time = time;
      });
  }

  void initState() {
    super.initState();
    _time = TimeOfDay.now();
  }

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      padding: const EdgeInsets.all(12.0),
      onPressed: _pickTime,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text('Напоминание в день годовщины',
              style: TextStyle(
                fontWeight: FontWeight.w600,
              )),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Text('В ${_time.hour}:${_time.minute}',
                style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.w300,
                )),
          )
        ],
      ),
    );
  }
}
