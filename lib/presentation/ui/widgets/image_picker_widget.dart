import 'dart:io';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:x/presentation/ui/pages/anniversary_creation/bloc/anniversary_creation_page_bloc.dart';

class ImagePickerWidget extends StatefulWidget {
  @override
  _ImagePickerWidgetState createState() => new _ImagePickerWidgetState();
}

class _ImagePickerWidgetState extends State<ImagePickerWidget> {
  File _image;
  final picker = ImagePicker();

  Future<void> getImage(BuildContext context) async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        context
            .read<AnniversaryCreationPageBloc>()
            .add(SetPhotoEvent(freindsPhoto: _image));
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      _image == null
          ? CircleAvatar(
              backgroundColor: Colors.amber.shade100,
              foregroundColor: Colors.grey,
              radius: 60.0,
              child: IconButton(
                onPressed: () {
                  getImage(context);
                },
                tooltip: 'Выбрать фото',
                icon: Icon(Icons.add_a_photo),
              ),
            )
          : CircleAvatar(
              backgroundImage: FileImage(_image),
              foregroundColor: Colors.amber.shade800,
              radius: 60.0,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: IconButton(
                  onPressed: () {
                    getImage(context);
                  },
                  tooltip: 'Выбрать фото',
                  icon: Icon(Icons.add_a_photo),
                ),
              )),
    ]);
  }
}
