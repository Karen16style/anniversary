import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:x/presentation/ui/pages/anniversary_creation/bloc/anniversary_creation_page_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DatePickerWidget extends StatefulWidget {
  @override
  _DatePickerWidgetState createState() => _DatePickerWidgetState();
}

class _DatePickerWidgetState extends State<DatePickerWidget> {
  final _wedingDayController = TextEditingController();

  DateTime wedingDayData;

  final DateFormat formatter = DateFormat('dd-MM-yyyy');

  Future<void> _selectDate(BuildContext context) async {
    wedingDayData = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1980),
      lastDate: DateTime(2050),
      helpText: 'Выберите дату годовщины',
    );
    if (wedingDayData != null) {
      setState(() {
        _wedingDayController.text = formatter.format(wedingDayData);
        context.read<AnniversaryCreationPageBloc>().add(SetDateEvent(anniversaryDate: wedingDayData));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: _wedingDayController,
      maxLength: 20,
      keyboardType: TextInputType.datetime,
      validator: (value) {
        if (value.isEmpty) {
          return 'Пожалуйста укажите дату';
        }
        return null;
      },
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          prefixIcon: Icon(Icons.event),
          suffixIcon: IconButton(
            icon: Icon(Icons.date_range),
            onPressed: () {
              _selectDate(context);
            },
          ),
          labelText: 'Дата годовщины*'),
    );
  }
}
