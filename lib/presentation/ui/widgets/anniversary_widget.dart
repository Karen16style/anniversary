import 'package:flutter/material.dart';
import 'package:x/data/entities/weding_day.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

class AnniversaryWidget extends StatefulWidget {
  final WedingDay wedingDay;

  const AnniversaryWidget({Key key, this.wedingDay}) : super(key: key);

  @override
  _AnniversaryWidgetState createState() => _AnniversaryWidgetState();
}

class _AnniversaryWidgetState extends State<AnniversaryWidget> {
  DateFormat dateFormat;
  DateFormat dateFormatDay;

  @override
  void initState() {
    super.initState();
    initializeDateFormatting();
    dateFormatDay = DateFormat.MMMMd('ru');
    dateFormat = DateFormat.MMMM('ru');
  }

  Widget build(BuildContext context) {
    String month = dateFormat.format(widget.wedingDay.anniversaryDate);
    String monthDay = dateFormatDay.format(widget.wedingDay.anniversaryDate);
    return Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(5),
        margin: EdgeInsets.symmetric(vertical: 5),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.symmetric(
              vertical: BorderSide.none,
              horizontal:
                  BorderSide(width: 1, color: Colors.grey.withOpacity(0.5))),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 1,
              offset: Offset(0, 1),
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 5.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('${month[0].toUpperCase()}${month.substring(1)}'),
                ],
              ),
              Row(
                children: [
                  CircleAvatar(
                    backgroundImage: AssetImage('assets/images/dash.jpg'),
                    radius: 60,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          '${widget.wedingDay.lastname}',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 10.0),
                        Text(
                          '${monthDay[0].toUpperCase()}${monthDay.substring(1)} ',
                          style: TextStyle(fontStyle: FontStyle.italic),
                        )
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ));
  }
}
