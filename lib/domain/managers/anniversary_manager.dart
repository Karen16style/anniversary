import 'package:x/data/entities/weding_day.dart';
import 'package:x/data/providers/firestore/firestore_provider.dart';

const anniversaryCollectionName = 'anniversary';

class AnniversaryManager {
  final FirestoreProvider _firestoreProvider;

  AnniversaryManager(this._firestoreProvider);

  Future<void> creatAnniversary(WedingDay wedingDay) {
    return _firestoreProvider.addDoc(
        collectionName: anniversaryCollectionName,
        data: wedingDay.toFirestore());
  }

  Stream<List<WedingDay>> readAnniversary() {
    return _firestoreProvider
        .readCollection(collectionName: anniversaryCollectionName)
        .map<List<WedingDay>>((snapshot) {
      final list = snapshot.docs
          .map<WedingDay>((document) =>
              WedingDay.fromFirestore(document.id, document.data()))
          .toList();
      final now = DateTime.now();
      final dateNew = <WedingDay>[];
      final dateOld = <WedingDay>[];

      list.forEach((w) {
        final d = w.anniversaryDate;
        if (now.isAfter(DateTime(
            now.year, d.month, d.day, now.hour, now.minute, now.second))) {
          dateOld.add(w);
        } else {
          dateNew.add(w);
        }
      });

      dateNew.sort((oldW, newW) {
        final newD = newW.anniversaryDate;
        final oldD = oldW.anniversaryDate;
        return newD.isBefore(DateTime(newD.year, oldD.month, oldD.day,
                newD.hour, newD.minute, newD.second))
            ? 1
            : newD.isAfter(DateTime(newD.year, oldD.month, oldD.day, newD.hour,
                    newD.minute, newD.second))
                ? -1
                : 0;
      });
      dateOld.sort((oldW, newW) {
        final newD = newW.anniversaryDate;
        final oldD = oldW.anniversaryDate;
        return newD.isBefore(DateTime(newD.year, oldD.month, oldD.day,
                newD.hour, newD.minute, newD.second))
            ? 1
            : newD.isAfter(DateTime(newD.year, oldD.month, oldD.day, newD.hour,
                    newD.minute, newD.second))
                ? -1
                : 0;
      });

      final newList = <WedingDay>[];
      newList.addAll(dateNew);
      newList.addAll(dateOld);
      return newList;
    }).asBroadcastStream();
  }
}
