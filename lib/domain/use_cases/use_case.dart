import 'package:x/domain/managers/anniversary_manager.dart';
import 'package:x/domain/managers/auth_manager.dart';
import 'package:x/data/providers/firestore/firestore_provider.dart';

class UseCase {
  AnniversaryManager _anniversaryManager;
  final AuthManager authManager = AuthManager();
  final FirestoreProvider _firestoreProvider = FirestoreProvider();
  AnniversaryManager get anniversary {
    if (_anniversaryManager == null) {
      _anniversaryManager = AnniversaryManager(_firestoreProvider);
    }
    return _anniversaryManager;
  }
}
