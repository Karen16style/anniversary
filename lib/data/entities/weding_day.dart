import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

class WedingDay {
  final String id;
  final String lastname;
  final DateTime anniversaryDate;
  final String surpise;
  final File freindsPhoto;

  WedingDay(
      {this.id,
      @required this.lastname,
      @required this.anniversaryDate,
      this.surpise,
      this.freindsPhoto});

  factory WedingDay.fromFirestore(String id, Map<String, dynamic> dataFF) {
    final lastname = dataFF['lastname'];
    final anniversaryDateRaw = dataFF['anniversaryDate'];
    DateTime anniversaryDate;
    if (anniversaryDateRaw is Timestamp) {
      anniversaryDate = anniversaryDateRaw.toDate();
    }
    final surpise = dataFF['surpise'];
    return WedingDay(
        id: id,
        lastname: lastname,
        anniversaryDate: anniversaryDate,
        surpise: surpise);
  }

  Map<String, dynamic> toFirestore() {
    final dataFF = <String, dynamic>{};
    if (lastname != null) dataFF['lastname'] = lastname;
    if (anniversaryDate != null)
      dataFF['anniversaryDate'] = Timestamp.fromDate(anniversaryDate);
    if (surpise != null) dataFF['surpise'] = surpise;
    return dataFF;
  }
}
