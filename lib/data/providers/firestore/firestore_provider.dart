import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

class FirestoreProvider {
  FirebaseFirestore _firestore = FirebaseFirestore.instance;
  Future<DocumentReference> addDoc(
      {@required String collectionName, @required Map<String, dynamic> data}) {
    return _firestore.collection(collectionName).add(data);
  }

  Future<DocumentReference> updateDoc(
      {@required String id,
      @required String collectionName,
      @required Map<String, dynamic> data}) {
    return _firestore.collection(collectionName).doc(id).set(data);
  }

  Future<DocumentReference> deleteDoc(
      {@required String id, @required String collectionName}) {
    return _firestore.collection(collectionName).doc(id).delete();
  }

  Stream<QuerySnapshot> readCollection({@required String collectionName}) {
    return _firestore
        .collection(collectionName)
        .orderBy('anniversaryDate')
        .snapshots();
  }
}
